// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr       : false,
    apiHost: 'https://apikori.junglerinnova.info:8080',
    firebase: {
        apiKey: 'AIzaSyDtjwlpDSk3bhF8L4B8JrpjjVYHC0OBiAg',
        authDomain: 'tribal-saga-231020.firebaseapp.com',
        databaseURL: 'https://tribal-saga-231020.firebaseio.com/',
        projectId: 'tribal-saga-231020',
        storageBucket: 'tribal-saga-231020.appspot.com',
        messagingSenderId: '516769543288'
    }
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
