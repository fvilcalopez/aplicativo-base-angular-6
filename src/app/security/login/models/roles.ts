export enum Role {
  Superadministrador = 1,
  Administrador = 2,
  Cashier = 3,
  Warehouse = 4
}

export namespace Rol {
  export function fromInt(s: number): Role{
    let  r: Role = null;
    for (let item in Role) {
      if(Role[item.toString()]  == s){
        r = Role[item.toString()] as Role;
        break;
      }
    }
    return r;
  }


  export function getName(role: Role): string{
    let name = ''; 
    switch (role) {
      case Role.Superadministrador:       
        name = "Superadministrador";
        break;
      case Role.Administrador:       
        name = "Administrador";
        break;
      case Role.Cashier:       
        name = "Encargado de Caja";
        break;
      case Role.Warehouse:       
        name = "Encargado de Almacén";
        break;
      default:
        break;
    }
    return name;
  }
}

