import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AuthenticationService} from './services/authentication.service';
import {FuseConfigService} from '../../../@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material';


@Component({templateUrl: 'login.component.html', styleUrls  : ['./login.component.scss'],
  animations : fuseAnimations})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  recuerdame = false;
  error: any;
  loginFormErrors: any;
  parent;

  onLoginFormValuesChanged()
  {
    for ( const field in this.loginFormErrors )
    {
      if ( !this.loginFormErrors.hasOwnProperty(field) )
      {
        continue;
      }

      // Clear previous errors
      this.loginFormErrors[field] = {};

      // Get the control
      const control = this.loginForm.get(field);

      if ( control && control.dirty && !control.valid )
      {
        this.loginFormErrors[field] = control.errors;
      }
    }
  }
  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private fuseConfig: FuseConfigService,
    private sanitizer: DomSanitizer,
    private authenticationService: AuthenticationService) {
    this.error = '';
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });
    this.loginFormErrors = {
      email: {},
      password: {}
    };
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email   : ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
      password: ['', [Validators.required, Validators.maxLength(10)]],
      recuerdame: [false, []]
    });

    // reset login status
    this.authenticationService.logout(true);
    // get return url from route parameters or default to '/'
    this.returnUrl = decodeURIComponent(this.route.snapshot.queryParams['returnUrl']) || '/app';
    this.loginForm.valueChanges.subscribe(() => {
      this.onLoginFormValuesChanged();
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  validarMaxLength() {
      if (this.loginForm.get('email').value.toString().length > 4) {
        this.loginForm.get('email').setValue(this.loginForm.get('email').value.toString().slice(0, 4 - (this.loginForm.get('email').value.toString().length)));
      }
      if (this.loginForm.get('password').value.toString().length > 10) {
        this.loginForm.get('password').setValue(this.loginForm.get('password').value.toString().slice(0, 10 - (this.loginForm.get('password').value.toString().length)));
      }
  }

  validarSoloNumeros(event: any) {
    const expRegNumeros = new RegExp('(?:.*\\d)(?:.*[A-Z])');
    const expRegLetras = new RegExp('(?:.*[a-z])(\\D+)');

    if (!expRegNumeros.test(this.loginForm.get('email').value)) {
      this.loginForm.get('email').setValue(this.loginForm.get('email').value.toString().replace(expRegLetras, ''));
    }
  }

  onKey(event: any) { // without type info
    this.validarMaxLength();
    this.validarSoloNumeros(event);
    // this.validarContraseña();
    // this.keyUp.emit({ value: this.parent.get(this.nombre).value, nombre: this.nombre });
  }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.iniciarSesion(this.f.email.value, this.f.password.value, this.recuerdame)
      .pipe(first())
      .subscribe(
        data => {
          this.sessionExitosa();
        },
        error => {
          console.log(error);
          if (error.error.poMensaje != null) {
            this.error = error.error.poMensaje;
          } else {
            (error.name == 'HttpErrorResponse') ? this.error = 'No hay conexión con los servidores. Porfavor revise su conexión a Internet.' : this.error = '';
          }
          this.f.email.setValue('');
          this.f.password.setValue('');
        });
  }



  sessionExitosa(){
    if (!this.returnUrl || this.returnUrl == 'undefined') { this.returnUrl = '/app/inicio/page01'; }
    const urls = this.returnUrl.split('?');
    const parms = {};
    if (urls.length > 1){
      const par = urls[1].split('&');
      for (let i = 0; i < par.length; i++){
        const p = par[i].split('=');
        parms[p[0]] = p[1];
      }
    }
    this.router.navigate([urls[0]], {queryParams: parms} );
  }

  
}
