import { Directive, ElementRef, OnInit , Input, Renderer2 } from '@angular/core';
import {Role} from "../models/roles";
import {AuthenticationService} from "../services/authentication.service";


@Directive({
  selector: '[ShowAuthorized]'
})
export class ShowAuthorizedDirective implements OnInit {

  private _permissions: Role[];
  @Input('ShowAuthorized') set permissions(permissions: Role[]){
    this._permissions = permissions;
    this.ngOnInit();
  }

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private authorizationService: AuthenticationService) {
    this.authorizationService.observable.subscribe(login => this.ngOnInit())
    setTimeout(() => this.ngOnInit(), 50);
  }
  ngOnInit() {
    if (!this.authorizationService.isLogin()){
      this.renderer.removeStyle(this.el.nativeElement, 'display');
    } else if(this._permissions==null) {
      this.renderer.setStyle(this.el.nativeElement, 'display', 'none');
    } else if(!this.authorizationService.hasRolPermision(this._permissions)){
      this.renderer.setStyle(this.el.nativeElement, 'display', 'none');
    } else {
      this.renderer.removeStyle(this.el.nativeElement, 'display');
    }
  }
}
