import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule, MatCheckboxModule, MatChipsModule, MatDialogModule, MatFormFieldModule, MatInputModule,
  MatRippleModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { LoginComponent} from './login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FuseDirectivesModule} from '../../../@fuse/directives/directives';
import {FusePipesModule} from '../../../@fuse/pipes/pipes.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HideUnauthorizedDirective} from './directives/hide-unauthorized.directive';
import {ShowAuthorizedDirective} from './directives/show-authorized.directive';
import {DisabledUnauthorizedDirective} from './directives/disabled-unauthorized.directive';
import {AsyncPipe} from '@angular/common';

@NgModule({
    declarations: [
        LoginComponent,
      HideUnauthorizedDirective,
      DisabledUnauthorizedDirective,
      ShowAuthorizedDirective
    ],
    imports     : [
      RouterModule,
      MatButtonModule,
      MatCheckboxModule,
      MatFormFieldModule,
      MatInputModule,
      MatChipsModule,
      FuseSharedModule,
      FlexLayoutModule,
      FuseDirectivesModule,
      FusePipesModule,
      MatRippleModule,
      BrowserAnimationsModule,
      MatDialogModule
    ],
  exports: [HideUnauthorizedDirective,
    ShowAuthorizedDirective,
    DisabledUnauthorizedDirective],
  providers: [AsyncPipe],
})
export class LoginModule
{
}
