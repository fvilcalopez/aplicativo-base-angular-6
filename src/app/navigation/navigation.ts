export const navigation = [
  {
    'id'      : 'dashboards',
    'title'   : 'Pizzaras Informativas',
    'type'    : 'group',
    'children': [
      {
        'id'   : 'indicadores',
        'title': 'Indicadores',
        'type' : 'item',
        'icon' : 'view_quilt',
        'url'  : '/app/reportes/indicadores'
      },
      {
        'id'   : 'desembolsos',
        'title': 'Desembolsos',
        'type' : 'item',
        'icon' : 'monetization_on',
        'url'  : '/app/reportes/desembolsos'
      },
      {
        'id'   : 'recuperaciones',
        'title': 'Recuperaciones',
        'type' : 'item',
        'icon' : 'gavel',
        'url'  : '/app/reportes/recuperaciones'
      }
    ]
  },
  {
    'id'      : 'configurations',
    'title'   : 'Configuraciones',
    'type'    : 'group',
    'children': [
      {
        'id'   : 'metas',
        'title': 'Metas',
        'type' : 'item',
        'icon' : 'trending_up',
        'url'  : '/app/configuraciones/metas'
      },
      {
        'id'   : 'catalogos',
        'title': 'Catalogos',
        'type' : 'item',
        'icon' : 'view_carousel',
        'url'  : '/app/configuraciones/catalogos'
      },
      {
        'id'   : 'geojson',
        'title': 'Editor de Territorios',
        'type' : 'item',
        'icon' : 'map',
        'url'  : '/app/configuraciones/geozonas'
      }
    ]
  }
];
