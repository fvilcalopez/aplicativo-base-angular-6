export const locale = {
    lang: 'es',
    data: {
        'NAV': {
            'PRODUCTS': 'Productos',
            'CLIENTS': 'Clientes',
            'SALES': 'Ventas',
            'OFFICES': 'Sucursales'

        }
    }
};
