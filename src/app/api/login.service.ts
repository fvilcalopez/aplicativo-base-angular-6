import { Injectable } from '@angular/core';
import {IRespuestaMensaje, IToken, IUsuarioIngreso} from './Model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = environment.apiHost;
  private pcUsuarioIngresodto: IUsuarioIngreso;
  constructor(private httpClient: HttpClient) {
    this.pcUsuarioIngresodto = new class implements IUsuarioIngreso {
      pcClave: string;
      pcFireToken: string;
      pcImei: string;
      pcIp: string;
      pcUsuario: string;
    };
  }

  public getAutorization(){
    let headers = new HttpHeaders();
    // let authorization: string = environment.jwt_client_id+":"+environment.jwt_client_secret;
    headers = headers.append('pcCliente', 'WMO ');
    return headers;
  }
  iniciarSesion(usuario: string, contraseña: string): Observable<IToken> {
    const headers = this.getAutorization();
    this.pcUsuarioIngresodto.pcClave = contraseña;
    this.pcUsuarioIngresodto.pcFireToken = 'token';
    this.pcUsuarioIngresodto.pcImei = '98756431232154';
    this.pcUsuarioIngresodto.pcIp = '192.68.1.1';
    this.pcUsuarioIngresodto.pcUsuario = usuario;

    // CAMBIA AQUI TU FUNCION DE INICIAR SESION EN EL BACK
    return this.httpClient.post<IToken>(this.url + '/RSSesion/mxIniciar' , this.pcUsuarioIngresodto, {
      headers
    });
  }



  refrescarSesion(refresh_token: string): Observable<IToken> {
    const headers = this.getAutorization();
    return this.httpClient.patch<IToken>(this.url + '/auth/refresh' , 
      {'grant_type': 'refresh_token',
      'token': refresh_token} , {
      headers
    });
  }
}
