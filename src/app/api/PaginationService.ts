import {HttpParams} from "@angular/common/http";
import {IPageRequest} from "./Model";

export class PaginationService {
  static getPageParms(page: IPageRequest): HttpParams{
    let params = new HttpParams();
    if (page != null) {
      if (page.all){
        params = params.set('todos', page.all + '');
      } else {
        params = params.set('size', page.size + '').set('page', page.number + '');
      }
      if (page.sort != null){
        if(page.sort.active)
          params = params.set('sort', page.sort.active+','+page.sort.direction );
      }
    }
    return params;
  }
}
