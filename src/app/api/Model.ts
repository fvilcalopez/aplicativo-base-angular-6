import {Sort} from '@angular/material';

export interface IModel {
  id: number;
}

export interface IModelTimestamp extends IModel {
  deleted_at: Date;
  updated_at: Date;
  created_at: Date;
}

export interface IPageable<T extends IModel>{
  data: T[];
  current_page: number;
  from: number;
  last_page: number;
  per_page: number;
  to: number;
  total: number;
}

export interface IPageSort{
  direction: string;
  property: string;
  ignoreCase: boolean;
  nullHandling: string;
  ascending: boolean;
  descending: boolean;
}

export interface IPageRequest{
  size: number;
  number: number;
  sort: Sort;
  all: boolean;

}

export interface IUser extends  IModelTimestamp{
  password: string;
  name: string;
  lastname: string;
  email: string;
  dni: string;
  address: string;
  phone: string;
  emergency: string;
}

export interface IRole extends  IModel{
  name: string;
}

export interface IRoleUser extends IModelTimestamp{
  user_id: number;
  role_id: number;
  role: IRole;
}

export interface IToken{
  pcTokenAcceso: string;
  pcTokenId: string;
  pcTokenRefresco: string;
  pcTokenFirebase: string;
}

export interface IMessage{
  message: string;
}

export interface IRespuestaMensaje{
  poMensaje: string;
  poEstadoHTTP: string;
}

export interface IUsuarioIngreso{
  pcClave: string;
  pcFireToken: string;
  pcImei: string;
  pcIp: string;
  pcUsuario: string;
}

export interface IDocumentType extends IModel{
  name: string;
}

export interface IMapa {
  jerarquia: string;  // Territorio
  nombre: string;  // Lima
  hijos: IGeoData[]; // LOS GEOJSON QUE ME TENGO
  dataMap: IGeoData;  // CONTORNO DE LIMA
}

export interface IPoints {
  jerarquia: string;  // Territorio
  nombre: string;  // Lima
  points: IPointsExportGeoJSON[]; // LOS GEOJSON QUE ME TENGO
}

export interface IGeoData {
  type: string;
  data: IGeoJson;
}

export interface IPointsExportGeoJSON {
  type: string;
  data: any;
}


export interface IDataPoints {
  type: string;
  data: IFeatureCollection;
}

export interface IFeatureCollection {
  type: string;
  features: IGeoJson[];
}

export interface IGeoJson {
  type: string;
  properties: IProperties;
  geometry: IGeometry;
}

export interface IProperties {
  name: string;
  description: string;
  color: string;
  opacity: number;
  color_liner: string;
  color_hover: string;
  opacity_hover: number;
}

export interface IGeometry {
  type: string;
  coordinates: any;
}
