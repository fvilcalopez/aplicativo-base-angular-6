import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfiguracionesComponent } from './configuraciones.component';
import { Wmo07Component } from './wmo07/wmo07.component';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '../../../../@fuse/shared.module';
import {AngularModulesModule} from '../../../general/angular-modules/angular-modules.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FuseSharedModule,
    AngularModulesModule
  ],
  declarations: [ConfiguracionesComponent, Wmo07Component]
})
export class ConfiguracionesModule { }
