import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'fuse-app-configuraciones',
  templateUrl: './configuraciones.component.html',
  styleUrls: ['./configuraciones.component.scss']
})
export class ConfiguracionesComponent implements OnInit {

  public glTitulo: string;
  public glCode: string;
  constructor(private router: Router,
              private route: ActivatedRoute) {
    this.route.url.subscribe(activeUrl => {
      this.glTitulo = this.route.snapshot.children[0].data.title;
      this.glCode = this.route.snapshot.children[0].data.code;
    });
  }

  ngOnInit() {

  }

}
