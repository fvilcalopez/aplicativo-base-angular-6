import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'fuse-app-wmo07',
  templateUrl: './wmo07.component.html',
  styleUrls: ['./wmo07.component.scss']
})
export class Wmo07Component implements OnInit {

  public glTitulo: string;
  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.glTitulo = data.title);
  }

}
