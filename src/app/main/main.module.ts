import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { MatSidenavModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseNavigationModule, FuseSearchBarModule, FuseShortcutsModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { FuseContentModule } from 'app/main/content/content.module';
import { FuseNavbarModule } from 'app/main/navbar/navbar.module';
import { FuseToolbarModule } from 'app/main/toolbar/toolbar.module';
import { FuseMainComponent } from './main.component';
import {ConfiguracionesComponent} from './content/configuraciones/configuraciones.component';
import {ConfiguracionesModule} from './content/configuraciones/configuraciones.module';
import {Wmo07Component} from './content/configuraciones/wmo07/wmo07.component';
const appRoutes: Routes = [
  {
    path: 'app',            // <---- parent component declared here
    component: FuseMainComponent,
    children: [
      {
        path: 'inicio',
        component: ConfiguracionesComponent,
        children: [
          {
            path: 'page01',
            component: Wmo07Component,
            data: {
              title: 'Metas',
              code: 'WMO-07'
            }
          }
        ]
      },
      { path: '', redirectTo: 'configuraciones', pathMatch: 'full' }
    ]
  },
];

@NgModule({
    declarations: [
        FuseMainComponent
    ],
    imports     : [
        // Material
        RouterModule.forChild(appRoutes),
        MatSidenavModule,
        // Modulos Nuestros
        ConfiguracionesModule,
        // Fuse
        FuseSharedModule,
        FuseThemeOptionsModule,
        FuseNavigationModule,
        FuseSearchBarModule,
        FuseShortcutsModule,
        FuseSidebarModule,
        FuseContentModule,
        FuseNavbarModule,
        FuseToolbarModule
    ],
    exports     : [
        FuseMainComponent
    ]
})
export class FuseMainModule
{
}
