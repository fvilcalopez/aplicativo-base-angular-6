FROM nginx

ENV APIHOSTENV $APIHOSTENV
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx.crt /etc/ssl/nginx.crt
COPY nginx/nginx.key /etc/ssl/nginx.key
COPY ./dist /usr/share/nginx/html

CMD for f in $(find /usr/share/nginx/html -regex '.*\.js'); \
      do envsubst '${APIHOSTENV},${FACEBOOKID},${ORCIDID},${GOOGLEID}' < $f > "$f.temporal"; \
        rm "$f"; \
        cp "$f.temporal" "$f"; \
        rm "$f.temporal"; \
    done;  exec nginx -g 'daemon off;'